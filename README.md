### Index ###
* Major Dependencies
* Design Consderations
* Deployment instructions
* Disclaimer

### Dependencies ###
* Django 2.2
* Celery
* Redis
* Psycopg2
* django-extra-views
* django-tables2
* crispy-forms



### Design Consderations ###

Story 1: 

Location: site root "/"

Simple upload form. Takes file uploads to the webserver
Webserver then stores this file and passes the filepath to a task
Task then opens this file, reads X number of lines and uses the file size to estimate total file length
Task then proceeds to read file line by line checking sku against database.
If sku exists. update entry Else create and save
While process is running save status updates to redis

Story 1 A)
Web worker running in the client webbrowser pings end point /state/<uuid>/
to query for the current running csv consumer task status. This also updates the progress bar.

On success page redirects to list.

I've also added a div that displays the percentage,messages and current state in a modal. The progress bar does work. It's just got some UI quirks.


WHY SSE WAS NOT USED:

Django natively does not support streaming. While it does have a streaming response it yields an entire function callstack back and is potentially not thread safe.

You can setup websockets via django-channels, however I felt a bidirectoinal communication between the server and client was over kill.


Story 2)

Location: /products/

list view of all products on the system with a filter and search in the nav bar


Story 3)

Delete all records is in the nav bar. Click to delete, redirects back to file upload

Story 4)


Click products from nav bar to access the Create button to redirect to product creation page

Click on any product row in the product list view to redirect to product edit page.


Story 5)

On update or create from the UI, a special django-signal is fired. This signal is listened to by a receiver that calls a fire_webhook task.

This task goes over all configured webhooks and either sends the message on create or update. the results are stored in REDIS and are not currently used.

Webhook configs can be accessed via the nav bar


### Deployment instructions ###

git push heroku master

### Disclaimer

Baring the used django packges, I have not written the jquery reuired to select a row and get the sku for the product edit page. nor have I written the jquery plugin required to create a new form on the webhook config page. This was take off stakeover flow comments.

Any javascript not written by me has been emedded in the html between script tags.

Any javascript that has been written by me is under the app.js or list.js files

I'm also not very good at UI frameworks but I am willing to learn :) !

