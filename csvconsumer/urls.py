from django.urls import path

from csvconsumer.views import CSVUploadFormProcessView,BaseFormTempalteView,CSVConsumerTaskStateView


urlpatterns = [
            path('',BaseFormTempalteView.as_view(),name="csv_upload_template"),
            path('upload/',CSVUploadFormProcessView.as_view(),name="csv_upload_consumer"),
            path('state/<str:task_id>/',CSVConsumerTaskStateView.as_view(),name="csv_upload_state"),
]
