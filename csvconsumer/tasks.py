from celery import shared_task
from django.urls import reverse

import csv,sys,os
from product.models import ProductModel as Product
from django.conf import settings


@shared_task(bind=True)
def process_csv(self,filepath):
    """
        Approximate number of lines in file by using filesize and 
        reading number of lines set in settings.
    """

    self.update_state(state="STARTED",meta={"message":"starting csv processing"})
    filesize = os.path.getsize(filepath)
    approx_num_lines=0
    self.update_state(state="PROGRESS",meta={"message":"Approximating number of lines in file"})
    approx_limit = settings.APPROX_LIMIT
    with open(filepath, newline='') as csvfile:
        product_reader = csv.reader(csvfile,delimiter=',')
        chunk_in_bytes = 0
        for index,row in enumerate(product_reader):
            line_size_bytes = sys.getsizeof(row)
            chunk_in_bytes += line_size_bytes  
            if index >= approx_limit:
                break
        approx_num_lines = approx_limit*filesize/chunk_in_bytes

    self.update_state(state="PROGRESS",meta={"data":{"approx_num_lines":approx_num_lines},"message":"Processing CSV"})

    """
        Processing the csv
    """
    with open(filepath, newline='') as csvfile:
        product_reader = csv.reader(csvfile,delimiter=',')
        next(product_reader, None)  # skip the headers
        last_index = 0
        for index,row in enumerate(product_reader):

            """
                Get Product by sku or create it
            """
            sku = row[1].upper()
            product, created = Product.objects.get_or_create(sku=sku)
            product.name = row[0]
            product.active = False
            product.description = row[2]
            product.save()
            """
                Update Task state
            """
            self.update_state(state="PROGRESS",meta={"data":{"approx_num_lines":approx_num_lines,"current_line_index":index},"message":"Processing CSV"})
            last_index = index
        
        result = {
                    "approx_num_lines":approx_num_lines,
                    "total_num_lines":last_index,
                    "success":reverse("product:paginated-list"),
                }
        return result
