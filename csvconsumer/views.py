from django.shortcuts import render
from django.views.generic.edit import BaseFormView
from django.views.generic import TemplateView
from django.views.generic.edit import FormMixin
from django.views.generic import View
from django.core.files.storage import FileSystemStorage
from django.http import JsonResponse

from celery import current_app

from csvconsumer.forms import CSVUploadForm
from csvconsumer.tasks import process_csv

import json


class BaseFormTempalteView(FormMixin,TemplateView):
    http_method_names = ['get',]
    template_name = 'csvconsumer/upload_form.html'
    form_class = CSVUploadForm

class CSVUploadFormProcessView(BaseFormView):

    http_method_names = ['post',]
    form_class = CSVUploadForm
    success_url = '/products/'


    def form_invalid(self, form):
        """If the form is invalid, render the invalid form."""
        response = {'error':form.errors}
        return JsonResponse(response)

    def form_valid(self, form):
        # This method is called when valid form data has been POSTed.
        # It should return an HttpResponse.
        csvfile = self.request.FILES["input_csv"]
        fs = FileSystemStorage()
        filename = fs.save(csvfile.name, csvfile)
        uploaded_file_url = fs.url(filename)
        result = process_csv.delay(uploaded_file_url)
        response = {
                    "task_id":str(result.task_id),
                }
        return JsonResponse(response)

class CSVConsumerTaskStateView(View):
    """
        Grabs task ID from url to query for AsyncTask result
    """
    http_method_names = ['get',]

    def get(self, request, *args, **kwargs):
        url_task_id = kwargs["task_id"]
        task = current_app.AsyncResult(url_task_id)
        response = {
                    "task_id":task.id,
                    'state': task.state,
                    'details': task.info,
                }
        if task.status == 'SUCCESS':
            response['results'] = task.get()
        return JsonResponse(response)

