from django.apps import AppConfig


class CsvconsumerConfig(AppConfig):
    name = 'csvconsumer'
