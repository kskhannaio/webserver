from django import forms


class CSVUploadForm(forms.Form):
    input_csv = forms.FileField()


