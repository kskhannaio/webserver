from django.apps import AppConfig


class WebhookConfig(AppConfig):
    name = 'webhook'


    def ready(self):
        import webhook.receivers
        import webhook.signals

