from celery import shared_task

from product.models import ProductModel as Product
from webhook.models import WebhookModel as WebhookConfig
from django.conf import settings
import requests

@shared_task(bind=True)
def fire_webhooks(self,sku,event_type):
    webhooks = None
    product = Product.objects.get(sku=sku)
    if event_type=="create":
        webhooks = WebhookConfig.objects.filter(on_create=True)
    elif event_type=="update":
        webhooks = WebhookConfig.objects.filter(on_update=True)
    for webhook in webhooks:
        payload = {
                    "sku":product.sku,
                    "event_type":event_type,
                  }
        response = requests.get(webhook.url, params=payload)
        return response






