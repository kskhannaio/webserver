from django.shortcuts import render

from extra_views import ModelFormSetView
from webhook.models import WebhookModel
# Create your views here.


class WebhookModelFormsetView(ModelFormSetView):
    model = WebhookModel
    fields = ['url', 'on_create','on_update']
    template_name = 'webhook/formset.html'
    factory_kwargs = {'extra': 1}
