from django.dispatch import receiver
from webhook.signals import ping_webhook
from product.models import ProductModel as Product
from webhook.tasks import fire_webhooks

@receiver(ping_webhook, sender=Product)
def webhook_url_task(sender, **kwargs):
    print("start_task")
    sku = kwargs["product_sku"]
    event_type = kwargs["event_type"]
    fire_webhooks.delay(sku=sku,event_type=event_type)
