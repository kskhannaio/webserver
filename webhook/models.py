from django.db import models

class WebhookModel(models.Model):
    url = models.URLField(max_length = 256) 
    on_create = models.BooleanField(default=False)
    on_update = models.BooleanField(default=False)

