from django.dispatch import Signal

ping_webhook = Signal(providing_args=["product_sku", "event_type"])
