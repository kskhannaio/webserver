from django.urls import path

from webhook.views import WebhookModelFormsetView
urlpatterns = [
    path('webhook/config/', WebhookModelFormsetView.as_view(),name="webhook-config"),
]
