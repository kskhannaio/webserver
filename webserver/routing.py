from channels.routing import ProtocolTypeRouter, URLRouter
import csvconsumer.routing

application = ProtocolTypeRouter({
    'http': URLRouter(csvconsumer.routing.urlpatterns),
})
