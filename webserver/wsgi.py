"""
WSGI config for webserver project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/2.2/howto/deployment/wsgi/
"""

import os

from django.core.wsgi import get_wsgi_application

settings_file = os.getenv('DJANGO_SETTINGS_MODULE', 'webserver.settings.local')
os.environ.setdefault('DJANGO_SETTINGS_MODULE', settings_file)

application = get_wsgi_application()
