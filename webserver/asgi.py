"""
ASGI entrypoint. Configures Django and then runs the application
defined in the ASGI_APPLICATION setting.
"""

import os
import django
from channels.routing import get_default_application

settings_file = os.getenv('DJANGO_SETTINGS_MODULE', 'webserver.settings.local')
os.environ.setdefault('DJANGO_SETTINGS_MODULE', settings_file)

django.setup()
application = get_default_application()
