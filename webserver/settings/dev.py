from webserver.settings.base import *

ALLOWED_HOSTS = ["fulfilio-csv-test.herokuapp.com",]

#DATABASES = {
#
#    'default': {
#        'ENGINE': 'django.db.backends.postgresql_psycopg2',
#        'NAME': 'deeg9gg8jbo3ci',
#        'USER': 'xocpuetjoujyjj',
#        'PASSWORD': 'b2bc3508380ce033bbc3eee7551f6934fd5cb764a1128cc88e1d9e2e2fa0e749',
#        'HOST': os.getenv('POSTGRES_URL', None),
#        'PORT': '5432',
#    }
#}

CELERY_BROKER_URL = os.getenv('REDIS_URL', None) 
CELERY_RESULT_BACKEND= os.getenv('REDIS_URL', None) 
APPROX_LIMIT=1

import django_heroku
django_heroku.settings(locals())

