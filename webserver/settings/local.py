from webserver.settings.base import *


# Database
# https://docs.djangoproject.com/en/2.2/ref/settings/#databases

CELERY_BROKER_URL = 'redis://localhost:6379'
CELERY_RESULT_BACKEND = 'redis://localhost:6379'
APPROX_LIMIT=1


 
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
    }
}


