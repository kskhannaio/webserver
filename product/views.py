from django.shortcuts import render
from django.contrib import messages
from django.views.generic import ListView,View,CreateView,UpdateView

from product.models import ProductModel as Product, ProductModelTable
from product.filters import ProductFilter
from product.forms import ProductModelForm

from django_filters.views import FilterView
from django_tables2.views import SingleTableMixin
from django.db.models import Q

from webhook.signals import ping_webhook


class ProductListView(SingleTableMixin, FilterView):
    table_class = ProductModelTable 
    queryset = Product.objects.all()
    template_name = "product/list.html"
    filterset_class = ProductFilter


    def get_queryset(self,**kwargs):
        """Search for text in GET query"""
        queryset = super().get_queryset(**kwargs)
        q = self.request.GET.get('q')
        if q is not None and q != '':
            queryset = queryset.filter(
                Q(name__icontains=q) | Q(description__icontains=q)
            )
        return queryset 
    def get_context_data(self, **kwargs):
        """Send search query to context to fill in UI"""
        context = super().get_context_data(**kwargs)
        q = self.request.GET.get('q')
        context['search'] = q 
        return context



from django.shortcuts import redirect,reverse

class ProductDeleteAllView(View):
    http_allowed_methods = ['get']

    def get(self, request, *args, **kwargs):

        products = Product.objects.all()
        product_count = products.count()
        products.delete()
        messages.success(self.request, '{num} products deleted'.format(num=product_count))
        return redirect("/")


class ProductUpdateView(UpdateView):
    model = Product
    template_name = "product/model_form.html"
    success_url = "/products/"
    slug_field = 'sku'
    slug_url_kwarg = 'sku'
    form_class = ProductModelForm

    def form_valid(self, form):
        result = super().form_valid(form)
        """If the form is valid, fire create webhook."""
        product = self.get_object()
        messages.success(self.request, 'Product {sku} updated'.format(sku=product.sku))
        ping_webhook.send(sender=product.__class__,product_sku=product.sku,event_type="update")
        return result

class ProductCreateView(CreateView):
    model = Product
    template_name = "product/model_form.html"
    success_url = "/products/"
    slug_field = 'sku'
    slug_url_kwarg = 'sku'
    form_class = ProductModelForm

    def form_valid(self, form):
        result = super().form_valid(form)
        """If the form is valid, fire create webhook."""
        product = self.object
        messages.success(self.request, 'Product {sku} created'.format(sku=product.sku))
        ping_webhook.send(sender=product.__class__,product_sku=product.sku,event_type="create")
        return result
