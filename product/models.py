import uuid
from django.db import models

# Create your models here.

class ProductModel(models.Model):
    uuid = models.UUIDField(editable=False,default=uuid.uuid4)
    name = models.CharField(max_length=256)
    sku  = models.SlugField(max_length=256,unique=True)
    active = models.BooleanField(default = False)
    description = models.TextField()


import django_tables2 as tables

class ProductModelTable(tables.Table):
    sku = tables.Column(attrs={'td': {'class': 'skucss'}})
    class Meta:
        model = ProductModel
        sequence = ('name', 'description', 'sku',"active")
        exclude = ("id","uuid")



