from django.urls import path

from product.views import ProductListView,ProductDeleteAllView,ProductCreateView,ProductUpdateView

urlpatterns = [
    path('products/', ProductListView.as_view(), name='paginated-list'),
    path('delete/', ProductDeleteAllView.as_view(), name='delete'),
    path('product/create/', ProductCreateView.as_view(), name='create'),
    path('product/update/<slug:sku>/', ProductUpdateView.as_view(), name='update'),
]
