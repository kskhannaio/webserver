import django_filters
from product.models import ProductModel as Product

class ProductFilter(django_filters.FilterSet):
    class Meta:
        model = Product
        fields = ['active',]
