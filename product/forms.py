from django.forms import ModelForm
from product.models import ProductModel as Product


class ProductModelForm(ModelForm):
    class Meta:
        model = Product
        fields = ["name","description","sku","active"]
