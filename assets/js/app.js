let asyncTask = null;
let task_id = null;
window.addEventListener( "load", function () {
  function sendData() {
    const XHR = new XMLHttpRequest();
    const FD = new FormData( form );
    XHR.addEventListener( "load", function(event) {
      let result_tag= document.getElementById( "task_id" );
      let response = JSON.parse(event.target.response);
      task_id = response["task_id"];
      result_tag.setAttribute("data-taskid",task_id);
      $('#progressModal').modal("toggle");
      asyncTask = setInterval( function() { queryTask(task_id); }, 500 );

    } );
    XHR.addEventListener( "error", function( event ) {
      alert( 'Oops! Something went wrong.' );
    } );
    XHR.open( "POST", "/upload/" );
    XHR.send( FD );
  }
 
  const form = document.getElementById( "csv_upload_form" );
  form.addEventListener( "submit", function ( event ) {
    event.preventDefault();
    sendData();
  } );
} );

function queryTask(e){
      let worker = new Worker('static/js/worker.js');
      worker.postMessage(e)
      //On state progress update divs
      //On State success stop interval task, redirect to success url
      worker.addEventListener('message', function(e) {
          let statediv = document.getElementById('state');
          let messagediv = document.getElementById('message');
          let percentagediv = document.getElementById('percentage');
          let percentagediv_2 = document.getElementById('percentage_2');

          statediv.innerHTML = e.data["state"];
          messagediv.innerHTML = e.data["message"];

          
          let percentage = Math.floor(e.data["percentage"]);
          if(percentage<100)
          {
          }
          else{
            percentage=99;
          }
          percentage = percentage.toString(10)+"%";

          percentagediv.innerHTML = percentage;
          percentagediv_2.innerHTML = percentage;
          percentagediv.style.width = percentage;
          percentagediv.setAttribute("aria-valuenow",percentage);
          switch(e.data["state"])
          {
              case 'PROGRESS':
                  break;
              case 'SUCCESS':
                  percentagediv.innerHTML = "100%";
                  clearInterval(asyncTask);
                  window.location.replace(e.data["success_url"]);
                  break;
              default:
                  break;
          };
  }, false);
}
