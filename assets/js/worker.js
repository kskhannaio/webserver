self.addEventListener('message', function(e) {

    let task_id = e.data;

    let result = "";
    let state = "";
    let info = "";
    let urlpath = "/state/" + task_id+ "/";

    const XHR = new XMLHttpRequest();
    XHR.addEventListener( "load", function(event) {

      console.log(event.target.responseText);
      const response = JSON.parse(event.target.responseText);
      const state = response["state"];
      const details = response["details"];
      let returndata = {};
      switch(state){
          case 'PENDING':
            break;
          case 'PROGRESS':
            const message = details["message"];
            const data = details["data"];
            const approx_num_lines = data["approx_num_lines"];
            const current_line_index = data["current_line_index"];
            const percentage = 100*current_line_index/approx_num_lines;
            returndata = {
                "message":message,
                "percentage":percentage,
                "state":state,
            }
            break;
          case 'SUCCESS':
            const success_url = details["success"];
            returndata = {
                    "state": state,
                    "message":"Finished",
                    "percentage":100,
                    "success_url":success_url,
              };
            break;
          default:
            returndata = {
                    "state": state,
                    "message":"Unknown state",
                    "percentage":0,
                    "success_url":"",
              };
    };
    self.postMessage(returndata);
    } );
    XHR.addEventListener( "error", function( event ) {
      console.log( 'Oops! Something went wrong.' );
    } );
    XHR.open( "GET", urlpath ,false);
    XHR.send(null);


}, false);
