self.addEventListener('message', function(e) {
    console.log(e.data);
    const formdata = new FormData(e.data);
    const XHR = new XMLHttpRequest();
    let task_id = null;
    let response = null;

    XHR.addEventListener( "load", function(event) {
      response = JSON.parse(event.target.response);
    } );
    XHR.addEventListener( "error", function( event ) {
      console.log( 'Oops! Something went wrong in form_worker' );
    } );
    XHR.open( "POST", "/upload/" );
    XHR.send( formdata );

    task_id = response["task_id"];
    if(task_id!==null)
    {
        self.postMessage(task_id);
    }



}, false);

