$(document).ready(function () {
    //Get slug/sku for row clicked 
    $('table:first').children('tbody:first').children('tr:first').css('background-color', '#0099ff');
    $('table tbody tr').bind("mouseover", function () {
        var colour = $(this).css("background-color");
        $(this).css("background", '#0099ff');

        $(this).bind("mouseout", function () {
            $(this).css("background", colour);
        });
    });
    //Onclick redirect to edit page
    $('table tbody tr').click(function () {
        let sku = $(this).closest('tr').find('td.skucss').text();
        const redirect_url  = "/product/update/" +sku+"/";
        window.location.replace(redirect_url);
    });
});
